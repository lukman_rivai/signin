<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \Firebase\JWT\JWT;

class BookController extends Controller
{
    public function book() {
        $data = "Data All Book";
        return response()->json($data, 200);
    }

    public function bookAuth() {
        $data = "Welcome " . Auth::user()->name;
        return response()->json($data, 200);
    }

    public function tokenMyLearning()
    {
    	$iat     = time();
    	$key     = "aPdSgVkYp3s6v9y/B?E(H+MbQeThWmZq";
    	$payload = array(
    	    "email" => "lukman.rivai7@gmail.com",
    	    "iat" => $iat
    	);

    	/**
    	 * IMPORTANT:ok
    	 * You must specify supported algorithms for your application. See
    	 // * https://tools.ietf.org/html/draft-ietf-jose-json-web-algorithms-40
    	 * for a list of spec-compliant algorithms.
    	 */
    	$jwt = JWT::encode($payload, $key);
    	$decoded = JWT::decode($jwt, $key, array('HS256'));


    	/*
    	 NOTE: This will now be an object instead of an associative array. To get
    	 an associative array, you will need to cast it as such:
    	*/

    	$decoded_array = (array) $decoded;

    	/**
    	 * You can add a leeway to account for when there is a clock skew times between
    	 * the signing and verifying servers. It is recommended that this leeway should
    	 * not be bigger than a few minutes.
    	 *
    	 * Source: http://self-issued.info/docs/draft-ietf-oauth-json-web-token.html#nbfDef
    	 */
    	JWT::$leeway = 60; // $leeway in seconds
    	$decoded = JWT::decode($jwt, $key, array('HS256'));
    	return $jwt;
    }
}
